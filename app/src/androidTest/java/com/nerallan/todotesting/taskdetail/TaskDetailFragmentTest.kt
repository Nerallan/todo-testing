package com.nerallan.todotesting.taskdetail

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.android.architecture.blueprints.todoapp.R
import com.nerallan.todotesting.ServiceLocator
import com.nerallan.todotesting.data.Task
import com.nerallan.todotesting.data.source.FakeAndroidTestRepository
import com.nerallan.todotesting.data.source.TasksRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@MediumTest // Marks the test as a "medium run-time" integration test, helps you group and choose which size of test to run
@RunWith(AndroidJUnit4::class) // Used in any class using AndroidX Test.
class TaskDetailFragmentTest {

    private lateinit var repository: TasksRepository

    @Before
    fun initRepository() {
        repository = FakeAndroidTestRepository()
        ServiceLocator.tasksRepository = repository
    }

    @After
    fun cleanupDataBase() = runBlockingTest {
        ServiceLocator.resetRepository()
    }

    @Test
    fun activeTaskDetails_DisplayedInUi() = runBlockingTest {
        // GIVEN - Add active (incomplete) task to the DB
        val activeTask: Task = Task("Active task", "AndroidX Rocks", false)
        repository.saveTask(activeTask)

        // WHEN - Details fragment launched and display active task
        // Creates a Bundle, which represents the fragment arguments for the task that get passed into the fragment).
        val bundle = TaskDetailFragmentArgs(activeTask.id).toBundle()
        // launchFragmentInContainer function creates a FragmentScenario, with this bundle and a theme.
        launchFragmentInContainer<TaskDetailFragment>(bundle, R.style.AppTheme)

        // THEN - Task details are displayed on the screen
        // make sure that the title/description are both shown and correct
        Espresso.onView(ViewMatchers.withId(R.id.task_detail_title_text)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.withText("Active task")))
        }

        Espresso.onView(ViewMatchers.withId(R.id.task_detail_description_text)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.withText("AndroidX Rocks")))
        }
        // make sure the "active" checkbox is shown unchecked
        Espresso.onView(ViewMatchers.withId(R.id.task_detail_complete_checkbox)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.isNotChecked()))
        }
    }

    @Test
    fun completedTaskDetails_DisplayedInUi() = runBlockingTest {
        // GIVEN - Add completed task to the DB
        val completedTask: Task = Task("Completed task", "AndroidX Rocks", true)
        repository.saveTask(completedTask)

        // WHEN - Details fragment launched to display task
        val bundle = TaskDetailFragmentArgs(completedTask.id).toBundle()
        launchFragmentInContainer<TaskDetailFragment>(bundle, R.style.AppTheme)

        // THEN - Task details are displayed on the screen
        // make sure that the title/description are both shown and correct
        Espresso.onView(ViewMatchers.withId(R.id.task_detail_title_text)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.withText("Completed task")))
        }

        Espresso.onView(ViewMatchers.withId(R.id.task_detail_description_text)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.withText("AndroidX Rocks")))
        }

        // make sure the "completed" checkbox is shown checked
        Espresso.onView(ViewMatchers.withId(R.id.task_detail_complete_checkbox)).apply {
            check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            check(ViewAssertions.matches(ViewMatchers.isChecked()))
        }
    }
}