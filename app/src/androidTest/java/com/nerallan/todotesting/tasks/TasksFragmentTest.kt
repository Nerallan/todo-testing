package com.nerallan.todotesting.tasks

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.example.android.architecture.blueprints.todoapp.R
import com.nerallan.todotesting.ServiceLocator
import com.nerallan.todotesting.data.Task
import com.nerallan.todotesting.data.source.FakeAndroidTestRepository
import com.nerallan.todotesting.data.source.TasksRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito


/**
 * In summary, to test navigation you can:
 *      1. Use Mockito to create a NavController mock.
 *      2. Attach that mocked NavController to the fragment.
 *      3. Verify that navigate was called with the correct action and parameter(s).
 */
@RunWith(AndroidJUnit4::class)
@MediumTest
@ExperimentalCoroutinesApi
class TasksFragmentTest {

    private lateinit var repository: TasksRepository

    @Before
    fun initRepository() {
        repository = FakeAndroidTestRepository()
        ServiceLocator.tasksRepository = repository
    }

    @After
    fun cleanupDb() = runBlockingTest {
        ServiceLocator.resetRepository()
    }

    @Test
    fun clickTask_navigateToDetailFragment() = runBlockingTest {
        repository.apply {
            saveTask(Task("title1", "description1", false, "id1"))
            saveTask(Task("title2", "description2", true, "id2"))
        }

        // GIVEN - On the home screen
        val scenario = launchFragmentInContainer<TasksFragment>(Bundle(), R.style.AppTheme)
        // use Mockito's mock fun to create a mock
        val navController = Mockito.mock(NavController::class.java)
        // need to associate navController with the fragment
        scenario.onFragment { taskFragment ->
            Navigation.setViewNavController(taskFragment.view!!, navController)
        }

        // WHEN - click on the first list item
        // RecyclerViewActions is part of the espresso-contrib library
        Espresso.onView(ViewMatchers.withId(R.id.tasks_list)).perform(
                RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(
                        ViewMatchers.hasDescendant(ViewMatchers.withText("title1")), click()
                )
        )

        // THEN - Verify that we navigate to the first default screen
        // Mockito's verify method is what makes this a mock — you're able to confirm the mocked navController
        // called a specific method (navigate) with a parameter
        // (actionTasksFragmentToTaskDetailFragment with the ID of "id1").
        Mockito.verify(navController).navigate(
                TasksFragmentDirections.actionTasksFragmentToTaskDetailFragment("id1")
        )
    }

    @Test
    fun clickAddTaskButton_navigateToAddEditFragment() {
        // GIVEN - On the home screen
        val scenario = launchFragmentInContainer<TasksFragment>(Bundle(), R.style.AppTheme)
        // use Mockito's mock fun to create a mock
        val navController = Mockito.mock(NavController::class.java)
        // need to associate navController with the fragment
        scenario.onFragment { taskFragment ->
            Navigation.setViewNavController(taskFragment.view!!, navController)
        }

        // WHEN - clicking on + button
        Espresso.onView(ViewMatchers.withId(R.id.add_task_fab)).perform(click())

        // THEN - Verify that we navigate to the add/edit fragment screen
        Mockito.verify(navController).navigate(
                TasksFragmentDirections.actionTasksFragmentToAddEditTaskFragment(
                        null, getApplicationContext<Context>().getString(R.string.add_task)
                )
        )
    }
}