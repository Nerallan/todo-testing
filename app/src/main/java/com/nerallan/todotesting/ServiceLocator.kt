package com.nerallan.todotesting

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.nerallan.todotesting.data.source.DefaultTasksRepository
import com.nerallan.todotesting.data.source.TasksDataSource
import com.nerallan.todotesting.data.source.TasksRepository
import com.nerallan.todotesting.data.source.local.TasksLocalDataSource
import com.nerallan.todotesting.data.source.local.ToDoDatabase
import com.nerallan.todotesting.data.source.remote.TasksRemoteDataSource
import kotlinx.coroutines.runBlocking

object ServiceLocator {

    private val lock = Any()

    private var database: ToDoDatabase? = null

    @Volatile
    var tasksRepository: TasksRepository? = null
        @VisibleForTesting set

    /**
     *  Provides an already existing repository or creates a new one.
     *  This method should be synchronized on this to avoid,
     *  in situations with multiple threads running, ever accidentally creating two repository instances.
     */
    fun provideTasksRepository(context: Context): TasksRepository {
        synchronized(this) {
            return tasksRepository ?: createTasksRepository(context)
        }
    }

    /**
     * creating a new repository.
     *
     * Will call @Link[createTaskLocalDataSource] and create a new @Link[TasksRemoteDataSource].
     */
    private fun createTasksRepository(context: Context): TasksRepository {
        val newRepository = DefaultTasksRepository(TasksRemoteDataSource, createTaskLocalDataSource(context))
        tasksRepository = newRepository
        return newRepository
    }

    /**
     *  creating a new local data source. Will call @Linki[createDataBase].
     */
    private fun createTaskLocalDataSource(context: Context): TasksDataSource {
        val database = database ?: createDataBase(context)
        return TasksLocalDataSource(database.taskDao())
    }

    /**
     *  creating a new database.
     */
    private fun createDataBase(context: Context): ToDoDatabase {
        val result = Room.databaseBuilder(
                context.applicationContext,
                ToDoDatabase::class.java,
                "Tasks.db"
        ).build()
        database = result
        return result
    }

    /**
     * Clears out the database and sets both the repository and database to null.
     * this method properly resets the @Link[ServiceLocator] state between tests.
     *
     * One of the downsides of using a service locator is that it is a shared singleton.
     * In addition to needing to reset the state of the service locator when the test finishes,
     * you cannot run tests in parallel.
     * to read more @see <a href="https://developer.android.com/training/dependency-injection#di-alternatives">di overview</a>
     */
    @VisibleForTesting
    fun resetRepository() {
        synchronized(lock) {
            runBlocking {
                TasksRemoteDataSource.deleteAllTasks()
            }
            // Clear all data to avoid test pollution.
            database?.apply {
                clearAllTables()
                close()
            }
            database = null
            tasksRepository = null
        }
    }
}