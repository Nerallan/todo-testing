package com.nerallan.todotesting

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
// A test class is just a normal class
class ExampleUnitTest {

    // Each test is annotated with @Test (this is a Junit annotation)
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun multiplication_isCorrect() {
        assertEquals(3, 2 * 2)
    }
}
