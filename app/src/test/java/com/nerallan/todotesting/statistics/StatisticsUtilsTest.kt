package com.nerallan.todotesting.statistics

import com.nerallan.todotesting.data.Task
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test

class StatisticsUtilsTest {

    /**
     *  naming convention for test function is
     *  subjectUnderTest_actionOrInput_resultState
     */
    @Test
    fun getActiveAndCompletedStats__noCompleted_returnsHundredZero() {
        // Create an active task
        val tasks = listOf(
                Task(
                        title = "some title",
                        description = "some description",
                        isCompleted = false)
        )
        // call function
        val statsResult = getActiveAndCompletedStats(tasks)
        // check the result
        assertEquals(statsResult.activeTasksPercent, 100f)
        assertEquals(statsResult.completedTasksPercent, 0f)
        // use Hamcrest readable assertions
        assertThat(statsResult.activeTasksPercent, `is`(100f))
        assertThat(statsResult.completedTasksPercent, `is`(0f))
    }

    @Test
    fun getActiveAndCompletedStats__noActive_returnsZeroHundred() {
        // Create an active task
        val tasks = listOf(
                Task(
                        title = "some title",
                        description = "some description",
                        isCompleted = true)
        )
        // When the list of tasks is computed with a completed task
        val statsResult = getActiveAndCompletedStats(tasks)
        // Then the percentages are 0 and 100
        assertThat(statsResult.completedTasksPercent, `is`(100f))
        assertThat(statsResult.activeTasksPercent, `is`(0f))
    }

    @Test
    fun getActiveAndCompletedStats_emptyList_returnsZeros() {
        val tasks = emptyList<Task>()
        val statsResult = getActiveAndCompletedStats(tasks)
        assertThat(statsResult.completedTasksPercent, `is`(0f))
        assertThat(statsResult.activeTasksPercent, `is`(0f))
    }

    @Test
    fun getActiveAndCompletedStats_nullList_returnsZeros() {
        val tasks = emptyList<Task>()
        val statsResult = getActiveAndCompletedStats(tasks)
        assertThat(statsResult.completedTasksPercent, `is`(0f))
        assertThat(statsResult.activeTasksPercent, `is`(0f))
    }

    @Test
    fun getActiveAndCompletedStats_halfCompleted_returns() {
        val tasks = listOf(
                Task(
                        title = "some title",
                        description = "some description",
                        isCompleted = false),

                Task(
                        title = "some title",
                        description = "some description",
                        isCompleted = true)
        )
        val statsResult = getActiveAndCompletedStats(tasks)
        assertThat(statsResult.completedTasksPercent, `is`(50f))
        assertThat(statsResult.activeTasksPercent, `is`(50f))
    }
}